# Инсталляция бесплатной, но ограниченной по ресурсам, базы данных Oracle 18c XE на сервер Centos 7

Здесь скрипты для поднятия чистой виртуальной машины. Ничего больше.

Подробнее <a href="https://oracle-dba.ru/database/installation/single-instance/centos/7/oracle/xe/18c/">здесь</a>

